Source: inadyn
Section: net
Priority: optional
Maintainer: Benda Xu <orv@debian.org>
Build-Depends:
 automake,
 debhelper-compat (= 13),
 libconfuse-dev,
 libgnutls28-dev,
 pkg-config
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://github.com/troglobit/inadyn
Vcs-Git: https://salsa.debian.org/debian/inadyn.git
Vcs-Browser: https://salsa.debian.org/debian/inadyn

Package: inadyn
Architecture: any
Pre-Depends: adduser, ${misc:Pre-Depends}
Depends: sysvinit-utils (>= 3.0.5), ${shlibs:Depends}, ${misc:Depends}
Provides: dyndns-client
Description: simple and small dynamic DNS client with HTTPS support
 It is used in routers and Internet gateways to automate the task of
 keeping your DNS record up to date with any IP address changes from
 your ISP. It can also be used in installations with redundant backup
 connections to the Internet.
 .
 Inadyn can maintain multiple host names with the same IP address, and
 has a HTTP/HTTPS based IP detection which runs well behind a NAT
 router.
 .
 Selected DDNS providers are supported natively, see inadyn(8) for the
 complete list. Other providers can be supported using the generic
 DDNS plugin. Some of these services are free of charge for
 non-commercial use. Others take a small fee but provide more domains
 to choose from.
